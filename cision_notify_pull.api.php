<?php

/**
 * @file
 * Hooks provided by the cision notify pull module.
 */

/**
 * Alter node object before it saved.
 *
 * @param object $node
 *
 * @ingroup cision_notify_pull
 */
function hook_cision_feed_node_alter(&$node) {

  /* Set node title
   * $node->set('title','cision title');
   */
}
