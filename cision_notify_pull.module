<?php

/**
 * @file
 * {@inheritdoc}
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\cision_notify_pull\Event\CisionEvent;
use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_help().
 */
function cision_notify_pull_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.cision_notify_pull':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('contains cision notify pull stuff.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_node_insert().
 */
function cision_notify_pull_node_insert(EntityInterface $node) {

  $node_array = $node->toArray();
  $config = \Drupal::service('config.factory')->get('cision_notify_pull.settings');
  $selected_type = $config->get('allowed_type');
  $target_mapping = $config->get('target_mapping');
  $debug = $config->get('debug');
  $languageVersion_field = array_search('LanguageVersions', $target_mapping);

  // If site is multilangual then we need to deal with different.
  // langauges from cision feeds.
  if (!empty($languageVersion_field) && $node_array['type'][0]['target_id'] == $selected_type && isset($node_array[$languageVersion_field][0]['value']) && !empty($node_array[$languageVersion_field][0]['value'])) {
    $entitytype_manager = \Drupal::service('entity_type.manager');
    $storageNode = $entitytype_manager->getStorage('node');
    $translated_node = $storageNode->loadByProperties([$languageVersion_field => $node_array[$languageVersion_field][0]['value']]);
    $translated_node = reset($translated_node);

    if ($translated_node && ($translated_node->language()->getId() != $node->language()->getId()) && !$translated_node->hasTranslation($node->language()->getId())) {
      if ($debug) {
        \Drupal::logger('cision_notify_pull')
          ->notice('translated node found @data', [
            '@data' => $node_array[$languageVersion_field][0]['value'],
          ]);
      }
      $translated_node->addTranslation($node->language()->getId(), $node_array)->save();
      \Drupal::service('event_dispatcher')
        ->dispatch('cision_notify_pull.delete.item.after.addtran', new CisionEvent($node));
    }
  }
}
